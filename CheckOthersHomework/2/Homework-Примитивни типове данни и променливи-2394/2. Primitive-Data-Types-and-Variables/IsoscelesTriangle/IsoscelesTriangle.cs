﻿using System;

class IsoscelesTriangle
    {
        static void Main()
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            int count = 0;
            int symbols = 9;
            int Base = 2 * symbols / 3 + 1;
            int baseSymbols = (Base+1) / 2;
            for (int i = baseSymbols; i > 0; i--)
            {
                string str = "";
                for (int j = Base; j > 0; j--)
                {
                    if ((i == j) || j == (Base - i+1) || (i == 1)&&((j % 2 != 0)||(((count+(j+1)/2)<symbols))))
                    {
                        str = str + (char)0x00A9;
                        count++;
                    }
                    else
                    {
                        str = str + " ";
                    }
                }
                Console.WriteLine(str);
            }
        }
    }
