﻿using System;

class ComparingFloats
    {
        static void Main()
        {
            Console.WriteLine("Type First Number");
            decimal a = decimal.Parse(System.Console.ReadLine());
            Console.WriteLine("Type Second Number");
            decimal b = decimal.Parse(System.Console.ReadLine());
            bool areEqual = decimal.Round(a * 1000000) == decimal.Round(b * 1000000);
            Console.WriteLine("Are the numbers equal? {0}", areEqual);
        }
    }

