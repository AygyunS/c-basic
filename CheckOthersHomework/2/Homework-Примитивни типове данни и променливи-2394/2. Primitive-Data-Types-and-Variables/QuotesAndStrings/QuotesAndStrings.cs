﻿using System;

class QuotesAndStrings
    {
        static void Main()
        {
            string sent1 = "The \"use\" of quotations causes difficulties.";
            string sent2 = @"The ""use"" of quotations causes difficulties.";
            Console.WriteLine(sent1);
            Console.WriteLine(sent2);
        }
    }
