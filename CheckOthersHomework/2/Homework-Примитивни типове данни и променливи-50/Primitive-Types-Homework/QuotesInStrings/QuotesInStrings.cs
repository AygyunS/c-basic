﻿using System;

class QuotesInStrings
{
    static void Main()
    {
        string withoutQuotedStrings = "The \"use\" of quotations causes difficulties.";
        string withQuotedStrings = @"The ""use"" of quotations causes difficulties.";
        Console.WriteLine(withoutQuotedStrings);
        Console.WriteLine(withQuotedStrings);
    }
}
