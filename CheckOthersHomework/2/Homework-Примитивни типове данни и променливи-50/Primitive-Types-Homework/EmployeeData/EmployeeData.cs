﻿using System;

    class EmployeeData
    {
        static void Main()
        {
            string firstName = "Pesho";
            string lastName = "Ivanov";
            byte age = 25;
            char gender = 'm';
            long personalID = 8306112507;
            int employeeNum = 27569999;
            Console.WriteLine("Name: {0} {1} \nAge: {2} Gender: {3} \nID: {4} \nEmployee Number: {5}", firstName,lastName, age, gender, personalID,employeeNum);
        }
    }