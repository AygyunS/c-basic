﻿using System;

class ComparingFloats
{
    static void Main()
    {
        double firstNum = 0.123456789;
        double secondNum = 0.123455227;
        double eps = 0.000001;
        double numsDifference = Math.Abs(firstNum - secondNum);
        bool compareNums = numsDifference < eps;
        Console.WriteLine(compareNums ? "Less than 0.000001 difference" : "More than 0.000001 difference");
    }
}
