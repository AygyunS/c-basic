﻿using System;

    class NullValuesAritmetic
    {
        static void Main()
        {
            int? valueInt = null;
            double? valueDouble = null;
            Console.WriteLine("Null integer: {0}, Null double: {1}", valueInt, valueDouble);
            valueInt += 1;
            valueDouble += 1;
            Console.WriteLine("After addition. \nNull integer: {0}, Null double: {1}", valueInt, valueDouble); //I never saw that comming!
        }
    }
