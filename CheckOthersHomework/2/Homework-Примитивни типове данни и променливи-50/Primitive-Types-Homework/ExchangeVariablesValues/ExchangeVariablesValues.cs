﻿using System;

class ExchangeVariablesValues
{
    static void Main()
    {
        int a = 5;
        int b = 10;
        for (int i = 0; i < 2; i++)
        {
            Console.WriteLine(a);
            Console.WriteLine(b);
            int c = a;
            a = b;
            b = c;
        }
    }
}
