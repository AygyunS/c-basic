﻿using System;
using System.Text;

class CopyrightSymbolTriangle
{
    static void Main()
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.WriteLine("   ©   ");
        Console.WriteLine("  © ©  ");
        Console.WriteLine(" ©   © ");
        Console.WriteLine("© © © ©");
    }
}
