﻿using System;

class PoinInsideCircleOutsideRectangle
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter value for X");
        double x = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter Value for Y");
        double y = double.Parse(Console.ReadLine());
        double r = 1.5;
        bool inCircle = r * r >= (x - 1) * (x - 1) + (y - 1) * (y - 1);
        bool inRectangle = (x >= -1) && (x <= 5) && (y >= -1) && (y <= 1);
        Console.WriteLine(inCircle && !inRectangle ? "The point is inside the circle and the rectangle" : "The point is outside");
    }
}

