﻿using System;

class BitwiseExtract
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter number");
        int a = int.Parse(Console.ReadLine());
        int b = 1 << 3;
        int c = a & b;
        if (c==0)
        {
            Console.WriteLine("Third bit is '0' " + "\n" + Convert.ToString(a, 2).PadLeft(16, '0'));
        }
        else{Console.WriteLine("Third bit is 1 "+"\n"+Convert.ToString(a,2).PadLeft(16,'0'));
        }
    }
}

