﻿using System;

class OddEvenIntegers
{
    static void Main()
    {

        Console.Write("Please enter number: ");
        {
            int Num = int.Parse(Console.ReadLine());
            bool oddEven = ((Num % 2) == 0);
            {
                Console.WriteLine(oddEven ? "The number is even" : "The number is odd");
            }
        }
    }
        
}

