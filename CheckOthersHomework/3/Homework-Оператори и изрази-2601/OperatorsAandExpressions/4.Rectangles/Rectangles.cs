﻿using System;

class Rectangles
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter width");
        int width= int.Parse(Console.ReadLine());
        Console.WriteLine("Enter height");
        int height = int.Parse(Console.ReadLine());
        int Perimeter = 2 * width + 2 * height;
        Console.WriteLine("The perimeter is "+Perimeter);
        int area = width * height;
        Console.WriteLine("The area is "+area);
    }
}

