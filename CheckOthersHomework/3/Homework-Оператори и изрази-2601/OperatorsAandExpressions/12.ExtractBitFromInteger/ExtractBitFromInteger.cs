﻿using System;

class ExtractBitFromInteger
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter number");
        int number = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter position to be extract");
        int position = int.Parse(Console.ReadLine());
        int newNumber = number >> position;
        int bit = newNumber & 1;
        Console.WriteLine(bit);
    }
}
