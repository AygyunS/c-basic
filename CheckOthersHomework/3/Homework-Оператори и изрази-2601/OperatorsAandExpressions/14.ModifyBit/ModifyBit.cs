﻿using System;

class ModifyBit
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter number");
        int number = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter position");
        int p = int.Parse(Console.ReadLine());
        Console.Write("v=");
        int v = int.Parse(Console.ReadLine());
        int newNumber = number & (~(1 << p)) | (1 << v);
        Console.WriteLine("The new number is {0}",newNumber);
    }
}
