﻿using System;

class GravitationOnTheMoon
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter a weight");
        float EarthWeight = float.Parse(Console.ReadLine());
        float MoonWeight = 17 * EarthWeight / 100;
        Console.WriteLine(MoonWeight);

    }
}

