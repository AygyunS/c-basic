﻿using System;

class CheckBitPosition
{
    static void Main(string[] args)
    {
        Console.WriteLine("Enter number");
        int number = int.Parse(Console.ReadLine());
        Console.WriteLine("Enter position");
        int p = int.Parse(Console.ReadLine());
        int newNumber = number >> p;
        int bit = newNumber & 1;
        bool result = (bit == 1);
        Console.WriteLine(result);
    }
}

