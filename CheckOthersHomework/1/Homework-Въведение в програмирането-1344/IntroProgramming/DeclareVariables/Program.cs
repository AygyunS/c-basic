﻿using System;

class DeclareVariables
{
    static void Main()
    {
        ushort firstValue = 52103;
        sbyte secondValue = 115;
        int thirdValue = 4825932;
        sbyte fourthValue = 97;
        short fifthValue = -10000;
        Console.WriteLine(" First value: {0}\n Second value:{1}\n Third value: {2}\n Fourth value: {3}\n Fifth value: {4}",firstValue,secondValue,thirdValue,fourthValue,fifthValue);
    } 
}

