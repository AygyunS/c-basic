﻿using System;

class PrintLongSequence
{
    static void Main()
    {
        int multiplier = 1;
        for (int i = 2; i <= 1000; i++)
        {
            Console.WriteLine(i * multiplier);
            multiplier = multiplier * -1;
            Console.SetBufferSize(Console.BufferWidth, 1002);
        }
    }
}

