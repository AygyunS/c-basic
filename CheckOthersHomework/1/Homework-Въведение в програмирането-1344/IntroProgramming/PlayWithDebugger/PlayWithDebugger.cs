﻿using System;

    class PlayWithDebugger
    {
        static void Main()
        {
            int  number = 0;
            for (int i = 0; i <= 1000; i++)
            {
                if (i == 200)
                {
                    Console.WriteLine("This is Breakpoint");
                }
                if (i % 1 == 0)
                {
                    number = i;
                }
                else
                {
                    number = i * (-1);
                }

                Console.WriteLine(number );
                Console.SetBufferSize(Console.BufferWidth, 1002);
            }
        }
    }

