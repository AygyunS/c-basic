﻿using System;

class DeclareVariables
{
    static void Main()
    {
        Console.Title = ("Five variables");

        byte bNum = 97;
        sbyte sbNum = -115;
        short shNum = -10000;
        ushort usNum = 52130;
        int iNum = 4825932;

        Console.WriteLine("For {0} - byte, {1} - sbyte, {2} - short, {3} - ushort, {4} - int.", bNum, sbNum, shNum, usNum, iNum);
    }
}