﻿using System;

class UnicodeValue
{
    static void Main()
    {
        char Unicode = 'H';
        int Num;
        Console.WriteLine("Char: " + Unicode);
        Console.WriteLine("Code: " + (int) Unicode);
        Num = (int)Unicode;
        Console.WriteLine("Hexadecimal: " + 0x72);
    }
}