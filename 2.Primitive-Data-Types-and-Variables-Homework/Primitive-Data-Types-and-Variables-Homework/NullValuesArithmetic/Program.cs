﻿using System;

class Program
{
    static void Main()
    {
        int? nullInt = null;
        double? nullDoub = null;
        Console.WriteLine("Nullable Int: {0}, Nullable Double: {1}", nullInt, nullDoub);
        nullInt = 335;
        nullDoub = 14.144;
        Console.WriteLine("Nullable Int(assigned val): {0}, Nullable Double(assigned val): {1}", nullInt, nullDoub);
    }
}