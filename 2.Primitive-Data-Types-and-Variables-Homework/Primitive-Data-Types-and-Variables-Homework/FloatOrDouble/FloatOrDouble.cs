﻿using System;

class FloatOrDouble
{
    static void Main()
    {
        double doubleNum = 34.567839023;
        float floatNum = 12.345f;
        double doubleNum2 = 8923.1234857;
        float floatNum2 = 3456.091f;

        Console.WriteLine("Values who can be assigned to float are:{0} and {1}", floatNum, floatNum2);
        Console.WriteLine("Values who can be assigned to double are:{0} and {1}", doubleNum, doubleNum2);
    }
}