﻿using System;

class BankAccountData
{
    static void Main()
    {
        string Fname, Mname, Lname, Bname, IBAN;
        decimal amount;
        ulong creditCard, debitCard, masterCard;

        Fname = "Aygyun";
        Mname = "Mithat";
        Lname = "Salim";
        Bname = "Първа Инвистационна Банка";
        IBAN = "BG20TTBB9753581353653";
        amount = 173376.344M;
        creditCard = 0117721143597945;
        debitCard = 4757966156773546;
        masterCard = 4426069517355568;

        Console.WriteLine("First Name: " + Fname + "\nMiddle Name: " + Mname + "\nLast Name: " + Lname + "\nBank Name: " + Bname + "\nIBAN: " + IBAN + "\nBalance: " + amount + "\nCreditCard: " + creditCard + "\nDebitCard: " + debitCard + "\nMasterCard: " + masterCard + "\n");

    }
}