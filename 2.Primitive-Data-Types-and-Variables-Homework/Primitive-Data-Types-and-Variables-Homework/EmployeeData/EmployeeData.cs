﻿using System;

class EmployeeData
{
    public static void Main()
    {
        string Fn, Ln;
        sbyte Age;
        char Sex;
        long ID;
        int employeeID;

        //Първи запис
        Fn = "Aygyun";
        Ln = "Salim";
        Age = 19;
        Sex = 'm';
        ID = 8736450923;
        employeeID = 27564567;
        Console.WriteLine("FirstName: " + Fn + "\nLastName: " + Ln + "\nAge: " + Age + "\nSex: " + Sex + "\nPersonal ID: " + ID + "\nEmployee ID: " + employeeID + "\n");

        //2-ри запис
        Fn = "Hristo";
        Ln = "Kirov";
        Age = 22;
        Sex = 'm';
        ID = 5247687455;
        employeeID = 788564225;
        Console.WriteLine("FirstName: " + Fn + "\nLastName: " + Ln + "\nAge: " + Age + "\nSex: " + Sex + "\nPersonal ID: " + ID + "\nEmployee ID: " + employeeID + "\n");

        //3-ти запис
        Fn = "Stanimir";
        Ln = "Ivanov";
        Age = 31;
        Sex = 'm';
        ID = 8896553255;
        employeeID = 27565554;
        Console.WriteLine("FirstName: " + Fn + "\nLastName: " + Ln + "\nAge: " + Age + "\nSex: " + Sex + "\nPersonal ID: " + ID + "\nEmployee ID: " + employeeID + "\n");

        //4-ти запис
        Fn = "Filip";
        Ln = "Kotev";
        Age = 25;
        Sex = 'm';
        ID = 5611139546;
        employeeID = 37564439;
        Console.WriteLine("FirstName: " + Fn + "\nLastName: " + Ln + "\nAge: " + Age + "\nSex: " + Sex + "\nPersonal ID: " + ID + "\nEmployee ID: " + employeeID + "\n");
    }
}