﻿using System;

class StringsAndObjects
{
    static void Main()
    {
        string Hello = "Hello";
        string World = "World";
        object obj1 = Hello + " " + World;
        string str1 = Convert.ToString(obj1);
        Console.WriteLine("{0}", str1);
    }
}
