﻿using System;

class QuotesInStrings
{
    static void Main()
    {
        string Quoting = "The \"use\" of quotations causes difficulties.";
        Console.WriteLine(Quoting);
        Quoting = "The use of quotations cause difficulties";
        Console.WriteLine(Quoting);
    }
}