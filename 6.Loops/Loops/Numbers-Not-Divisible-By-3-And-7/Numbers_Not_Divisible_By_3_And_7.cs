﻿using System;

class Numbers_Not_Divisible_By_3_And_7
{
    static void Main()
    {
        Console.Write("Enter number:");
        int number = int.Parse(Console.ReadLine());
        
        for (int i = 1; i < number; i++)
        {
            if ((i % 7 == 0) || (i % 3 == 0))
            {
                continue;
            }
            Console.WriteLine(i);
        }
    }
}