﻿using System;

class Numbers_From_1_To_N
{
    static void Main()
    {
        Console.Write("Enter number N to print all numbers from 1 to N\nN = ");
        int n = int.Parse(Console.ReadLine());

        for (int i = 1; i < n; i++)
        {
            Console.WriteLine(i);
        }
        Console.WriteLine(n);
        Console.WriteLine("N = {0}", n);
    }
}