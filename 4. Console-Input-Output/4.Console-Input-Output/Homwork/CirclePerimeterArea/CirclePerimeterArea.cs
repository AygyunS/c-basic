﻿using System;

class CirclePerimeterArea
{
    static void Main()
    {
        int r;
        double A;
        Console.WriteLine("Enter the radius: ");
        r = Convert.ToInt32(Console.ReadLine());
        A = (3.14) * r * r;
        Console.WriteLine("The area of circle of given is=" + A);
        Console.ReadLine();
    }
}