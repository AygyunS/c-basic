﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class PrintCompanyInformation
{
    static void Main()
    {
        Console.WriteLine("Insert company name:");
        string company = Console.ReadLine();
        Console.WriteLine("Insert company phone number:");
        int phoneCompany = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert company fax number:");
        int faxCompany = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert company web site:");
        string webSite = Console.ReadLine();
        Console.WriteLine("Insert company manager's first name:");
        string managertName = Console.ReadLine();
        Console.WriteLine("Insert company manager's last name:");
        string LastName = Console.ReadLine();
        Console.WriteLine("Insert company manager's age:");
        int ageManager = int.Parse(Console.ReadLine());
        Console.WriteLine("Insert company manager's phone number:");
        int numberManager = int.Parse(Console.ReadLine());
        Console.WriteLine("Company information: {0}, phone number: {1}, fax: {2}, web site: {3}.",
            company, phoneCompany, faxCompany, webSite);
        Console.WriteLine("Manager: {0} {1}.", managerName, LastName);
        Console.WriteLine("Manager age: {0}, phone number: {1}.", ageManager, numberManager);
    }
}