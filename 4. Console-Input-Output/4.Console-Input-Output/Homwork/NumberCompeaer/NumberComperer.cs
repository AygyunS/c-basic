﻿using System;

class NumberComperer
{
    static void Main()
    {
        Console.WriteLine("Enter your first number: ");
        double numb1 = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter your secend number: ");
        double numb2 = double.Parse(Console.ReadLine());
        double greaternum = Math.Max(numb1, numb2);
        Console.WriteLine("Greater number is: " + greaternum);
    }
}