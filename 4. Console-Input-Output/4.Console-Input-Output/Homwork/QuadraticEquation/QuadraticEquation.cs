﻿using System;

class QuadraticEquation
{
    static void Main()
    {
        Console.WriteLine("Enter number a: ");
        double numA = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter number b: ");
        double numB = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter number c: ");
        double numC = double.Parse(Console.ReadLine());
        double descriminant = Math.Pow(numB, 2) - (4* numA * numC);
        double x;
        double x1;
        double x2;
        if (descriminant == 0)
        {
            x = (-numB) / (2 * numA);
            Console.WriteLine("There is only one root x1=x2= {0}", x);
        }
        else if (descriminant < 0)
        {
            Console.WriteLine("There are no real roots.");
        }
        else
        {
            x1 = (-numB - Math.Sqrt(descriminant)) / (2 * numA);
            x2 = (-numB + Math.Sqrt(descriminant)) / (2 * numA);
            Console.WriteLine("Root x1 = {0}. Root x2= {1}.", x1, x2);
        }
    }
}