﻿using System;

class Sum3Integers
{
    static void Main()
    {
        Console.WriteLine("Enter first number: ");
        double num1 = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter secend number: ");
        double num2 = double.Parse(Console.ReadLine());
        Console.WriteLine("Enter third number: ");
        double num3 = double.Parse(Console.ReadLine());
        double sum = num1 + num2 + num3;
        Console.WriteLine("The sum of three numbers is: {0}", sum);
    }
}