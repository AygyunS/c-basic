﻿using System;

class FormattingNumbers
{
    static void Main()
    {
        int num1 = int.Parse(Console.ReadLine());
        float num2 = float.Parse(Console.ReadLine());
        float num3 = float.Parse(Console.ReadLine());
        string binary = Convert.ToString(num1, 2);
        if (num1 < 500 & num1 >= 0)
        {
            Console.WriteLine("{0,-10:X}|{1}|{2,10:F2}|{3,-10:0.000}|"
                , num1, binary.PadLeft(10, '0'), num2, num3);
        }
        else
        {
            Console.WriteLine("Number a is not between 0 ≤ a ≤ 500.");
        }

    }
}