﻿using System;

class Numbers
{
    static void Main()
    {
        Console.WriteLine("Enter n: ");
        int num = int.Parse(Console.ReadLine());
        Console.WriteLine("");
        for (int i = 1; i < num; i++)
        {
            Console.WriteLine(i);
        }
    }
}
