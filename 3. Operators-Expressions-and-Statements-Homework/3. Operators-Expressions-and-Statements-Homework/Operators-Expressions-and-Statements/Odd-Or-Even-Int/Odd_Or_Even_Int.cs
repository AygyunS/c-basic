﻿using System;

class Odd_Or_Even_int
{
    static void Main()
    {
        string number = null;
        Console.WriteLine("Enter number: ");
            number = Console.ReadLine();
        int Intnumber = int.Parse(number);
        int express = Intnumber % 2;
        string result = express == 0 ? "Even" : "Odd";
        Console.WriteLine("The number is {0}", result);

    }
}