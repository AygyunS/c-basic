﻿using System;

class Gravi_On_The_Moon
{
    static void Main()
    {
        Console.WriteLine("Enter man's weight on the Earth: ");
        decimal earthWeight = decimal.Parse(Console.ReadLine());
        decimal moonWeight = earthWeight * 0.17m;
        Console.WriteLine("The man's weight on the Moon will be:\n" + moonWeight);
    }
}