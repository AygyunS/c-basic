﻿using System;

class Divide_By_7_And_5
{
    static void Main()
    {
        Console.WriteLine("Enter an integers number: ");
        int number = int.Parse(Console.ReadLine());
        bool isDivide = number % 35 == 0;
        Console.WriteLine("Is the number divided (without remainder) by 7 and 5 in the same time?\n{0}", isDivide);
    }
}