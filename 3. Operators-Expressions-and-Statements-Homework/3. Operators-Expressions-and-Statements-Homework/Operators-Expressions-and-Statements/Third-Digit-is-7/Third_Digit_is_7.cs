﻿using System;

class Third_Digit_is_7
{
    static void Main()
    {
        Console.WriteLine("Enter an integer number: ");
        int number = int.Parse(Console.ReadLine());
        int path = number / 100;
        int third = path % 10;
        bool seven = third == 7;
        Console.WriteLine("Is the third digit 7?\n" + seven);
    }
}