﻿using System;

class FourDigitNumber
{
    static void Main()
    {
        Console.WriteLine("Enter four-digit number:");
        string number = Console.ReadLine();
        while (number.StartsWith("0") || number.Length != 4)
        {
            Console.WriteLine("The number must be exactly 4 digits and cannot start with 0. Please, try again!");
            number = Console.ReadLine();
        }
        int sum = 0;
        string numReverse = "r";
        for (int i = 0; i < number.Length; i++)
        {
            sum += int.Parse(number[i].ToString());
            numReverse = numReverse.Insert(0, number[i].ToString());
        }
        numReverse = numReverse.Remove(4, 1);
        Console.WriteLine(sum);
        Console.WriteLine(numReverse);
        string lastAtFirstPosition = (number.Insert(0, number[3].ToString())).Remove(4);
        Console.WriteLine(lastAtFirstPosition);
        string exchangeSecondThird = (number.Insert(1, number[2].ToString())).Remove(3, 1);
        Console.WriteLine(exchangeSecondThird);
    }
}