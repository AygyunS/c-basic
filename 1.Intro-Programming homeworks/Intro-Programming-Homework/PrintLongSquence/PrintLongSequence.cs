﻿using System;

class PrintLongSequence
{
    static void Main()
    {
        Console.Write(2);
        for (int i = 2; i <= 1000; i++)
        {
            int member = i + 1;
            if (member % 2 != 0)
            {
                member = -member;
            }
            Console.Write(", " + member);
        }
 
        Console.WriteLine(".");
    }
}