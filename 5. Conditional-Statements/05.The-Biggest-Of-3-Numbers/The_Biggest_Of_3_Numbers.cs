﻿using System;

class The_Biggest_Of_3_Numbers
{
    static void Main()
    {
        Console.WriteLine("Enter tree numbers to compare them:");
        Console.Write("A = ");
        decimal first = decimal.Parse(Console.ReadLine());
        Console.Write("B = ");
        decimal secend = decimal.Parse(Console.ReadLine());
        Console.Write("C = ");
        decimal third = decimal.Parse(Console.ReadLine());

        if (first > secend && first > third)
        {
            Console.WriteLine("The bigger number is: " + first);
        }
        else if (secend > first && secend > third)
        {
            Console.WriteLine("The bigger number is: " + secend);
        }
        else
        {
            Console.WriteLine("The bigger number is: " + third);
        }
        
    }
}