﻿using System;

class Play_With_Int_Double_String
{
    static void Main()
    {
        Console.WriteLine("Please choose a type:");
        Console.WriteLine("1 --> int"); 
        Console.WriteLine("2 --> double");
        Console.WriteLine("3 --> string");
        int all = int.Parse(Console.ReadLine());
        
        switch(all)
        {
            case 1:
                Console.Write("Enter a int: ");
                int num = int.Parse(Console.ReadLine());
                Console.WriteLine(num + 1);
                break;
            case 2:
                Console.WriteLine("Enter a double: ");
                double num2 = double.Parse(Console.ReadLine());
                Console.WriteLine(num2 + 1);
                break;
            case 3:
                Console.WriteLine("Enter a string: ");
                string word = Console.ReadLine();
                Console.WriteLine(word + "*");
                break;

        }
    }
}