﻿using System;

    class Sort_3_Numbers_with_Nested_ifs
    {
        static void Main()
        {
            Console.WriteLine("Enter three real numbers: ");
            Console.Write("A = ");
            decimal a = decimal.Parse(Console.ReadLine());
            Console.Write("B = ");
            decimal b = decimal.Parse(Console.ReadLine());
            Console.Write("C = ");
            decimal c = decimal.Parse(Console.ReadLine());
            if (a > b && a > c)
            {
                if (b > c)
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", c, b, a);
                }
                else 
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", b, c, a);
                }
            }
            else if (b > a && b > c)
            {
                if (a > c)
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", c, a, b);
                }
                else
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", a, c, b);
                }
            }
            else
            {
                if (a > b)
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", b, a, c);
                }
                else 
                {
                    Console.WriteLine("Result: {0}, {1}, {2}", a, b, c);
                }
            }
            
        }
    }