﻿using System;

    class The_Biggest_Of_Five_Numbers
    {
        static void Main()
        {
            Console.WriteLine("Enter five variables: ");
            Console.Write("A = ");
            decimal a = decimal.Parse(Console.ReadLine());
            Console.Write("B = ");
            decimal b = decimal.Parse(Console.ReadLine());
            Console.Write("C = ");
            decimal c = decimal.Parse(Console.ReadLine());
            Console.Write("D = ");
            decimal d = decimal.Parse(Console.ReadLine());
            Console.Write("E = ");
            decimal e = decimal.Parse(Console.ReadLine());
            if (a > b && a > c && a > d && a > e)
            {
                Console.WriteLine("The biggest number is: " + a);
            }
            else if (b > a && b > c && b > d && b > e)
            {
                Console.WriteLine("The biggest number is : " + b);
            }
            else if (c > a && c > b && c > d && c > e)
            {
                Console.WriteLine("The biggest number is: " + c);
            }
            else if (d > a && d > b && d > c && d > e)
            {
                Console.WriteLine("The biggest number is: " + d);
            }
            else if (e > a && e > b && e > c  && e > d)
            {
                Console.WriteLine("The biggest number is: " + e);
            }
        }
    }